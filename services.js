const {
    sendRequest
} = require('./http-client');
const {dbToInterfaceUser} = require("./utils");
const managementAxios = require("./auth0-management");
const ioRoute = process.env.IO_SERVICE_API_ROUTE;

const getUsers = async () => {
    console.info(`Sending request to IO for all users ...`);

    const options = {
        url: `http://${ioRoute}/users`
    }

    const users = await sendRequest(options);

    return users.map(dbToInterfaceUser);
};

const getUserById = async (id) => {
    console.info(`Sending request to IO for user ${id} ...`);

    const options = {
        url: `http://${ioRoute}/users/${id}`
    }

    const user = await sendRequest(options);
    return dbToInterfaceUser(user);
};


const addUser = async (id, username, picture, email, type, isUserVerified, isEnabled) => {
    console.info(`Sending request to IO to add user with user_id ${id}, username ${username}, picture ${picture}, email ${email}, type ${type} ...`);
    const options = {
        url: `http://${ioRoute}/users`,
        method: 'POST',
        data: {
            id,
            username,
            picture,
            email,
            type,
            isUserVerified,
            isEnabled
        }
    }

    const user_id = await sendRequest(options);

    return user_id;
};

const updateUser = async (id, username, picture, email) => {
    console.info(`Sending request to IO to update user ${id} with username ${username}, picture ${picture}, email ${email}...`);

    const options = {
        url: `http://${ioRoute}/users/${id}`,
        method: 'PUT',
        data: {
            username,
            picture,
            email
        }
    }

    const user = await sendRequest(options);

    return null;
};

const changeUserRole = async (id, type) => {
    console.info(`Sending request to IO to change type of user ${id} to ${type}...`);
    const roleId = {
        USER: 'rol_k2Bvy96wWc0282MO',
        ADMIN: 'rol_ubeQKB3wt11tLh8n',
    };

    await managementAxios.delete(`https://dev-vlziad43.eu.auth0.com/api/v2/users/${id}/roles`, { data: { roles: [roleId.ADMIN, roleId.USER] } });
    await managementAxios.post(`https://dev-vlziad43.eu.auth0.com/api/v2/users/${id}/roles`, { roles: [roleId[type]] });

    const options = {
        url: `http://${ioRoute}/users/${id}/change-role/${type}`,
        method: 'PUT',
        data: {
            type
        }
    }

    const user = await sendRequest(options);

    return null;
};

const changeUserEnabled = async (id, isEnabled) => {
    console.info(`Sending request to IO to change is_enabled of user ${id} to ${isEnabled}...`);

    const options = {
        url: `http://${ioRoute}/users/${id}/change-enabled/${isEnabled}`,
        method: 'PUT',
        data: {
            isEnabled
        }
    }

    const user = await sendRequest(options);

    return null;
};

const changeUserVerified = async (id, isUserVerified) => {
    console.info(`Sending request to IO to change is_verified of user ${id} to ${isUserVerified}...`);

    const options = {
        url: `http://${ioRoute}/users/${id}/change-verified/${isUserVerified}`,
        method: 'PUT',
        data: {
            isUserVerified
        }
    }

    const user = await sendRequest(options);

    return null;
};

module.exports = {
    getUserById,
    getUsers,
    addUser,
    updateUser,
    changeUserRole,
    changeUserEnabled,
    changeUserVerified
}