const Router = require('express').Router();
const validator = require('express-joi-validation').createValidator({})
const {
    getUsers,
    getUserById,
    addUser,
    changeUserRole,
    updateUser,
    changeUserEnabled,
    changeUserVerified
} = require('./services.js');

const isAdmin = require('./is-admin');
const {addUserSchema, editUserSchema, changeUserRoleSchema, changeUserEnabledSchema, changeUserVerifiedSchema} = require("./utils");
const { getUserIdFromToken, } = require("./utils");

Router.get('/', async (req, res) => {

    const users = await getUsers();

    res.json(users);
});

Router.get('/current', async (req, res) => {
    const id = getUserIdFromToken(req)
    console.log(id)
    let user = await getUserById(id);
    console.log("User : ", user)
    if (!user.id) user = await addUser(id, null, null, null, 'USER', false, false)

    res.json(user);
});

Router.post('/', validator.body(addUserSchema), async (req, res) => {
    const {
        id,
        username,
        picture,
        email,
        type,
        isUserVerified,
        isEnabled
    } = req.body;

    const user_id = await addUser(id, username, picture, email, type, isUserVerified, isEnabled);

    res.json(user_id);
});

Router.put('/:id', validator.body(editUserSchema), async (req, res) => {
    const {
        id
    } = req.params;

    const {
        username,
        picture,
        email
    } = req.body;

    const user = await updateUser(id, username, picture, email);

    res.json(user);
});

Router.post('/:id/change-role/:role', validator.body(changeUserRoleSchema), async (req, res) => {
    const { id, role } = req.params;

    await changeUserRole(id, role);

    res.json(null);
});

Router.post('/:id/change-enabled/:isEnabled', validator.body(changeUserEnabledSchema), async (req, res) => {
    const { id, isEnabled } = req.params;

    await changeUserEnabled(id, isEnabled);

    res.json(null);
});

Router.post('/:id/change-verified/:isVerified', validator.body(changeUserVerifiedSchema), async (req, res) => {
    const { id, isVerified } = req.params;

    await changeUserVerified(id, isVerified);

    res.json(null);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;
    console.log(id)
    const user = await getUserById(id);
    console.log(user);
    res.json(user);
});

module.exports = Router;