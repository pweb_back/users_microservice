const Joi = require("joi");
const dbToInterfaceUser = (user) => {
    return {
        id: user.user_id,
        username: user.username,
        picture: user.picture,
        email: user.email,
        type: user.usertype,
        isUserVerified: user.is_verified,
        isEnabled: user.is_enabled
    }
};

const addUserSchema = Joi.object({
    id: Joi.string().required(),
    username: Joi.string(),
    picture: Joi.string(),
    email: Joi.string().email(),
    type: Joi.string().valid('ADMIN', 'USER').uppercase().required(),
    isUserVerified: Joi.bool().required(),
    isEnabled: Joi.bool().required(),
});

const editUserSchema = Joi.object({
    username: Joi.string().required(),
    picture: Joi.string(),
    email: Joi.string().email().required(),
});

const changeUserRoleSchema = Joi.object({
    type: Joi.string().valid('ADMIN', 'USER').uppercase().required(),
});

const changeUserEnabledSchema = Joi.object({
    isEnabled: Joi.bool().required()
});

const changeUserVerifiedSchema = Joi.object({
    isVerified: Joi.bool().required()
});

const getUserIdFromToken = (req) => {
    const auth = req.header('Authorization');
    const token = auth.replace('Bearer ', '');
    const claimsBase64 = token.split('.')[1];
    const claims = JSON.parse(Buffer.from(claimsBase64, 'base64').toString());
    return claims.sub;
}

module.exports = {
    dbToInterfaceUser,
    getUserIdFromToken,
    addUserSchema,
    editUserSchema,
    changeUserRoleSchema,
    changeUserEnabledSchema,
    changeUserVerifiedSchema
}